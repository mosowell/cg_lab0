﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        List<Point> points = new List<Point>();
        Bitmap bitmap;
        public Form1()
        {
            InitializeComponent();
            bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            Graphics g = Graphics.FromImage(bitmap);

            if (e.Button == MouseButtons.Left)
            {
                points.Add(new Point(e.X, e.Y));
                Pen blackBrush = new Pen(new SolidBrush(Color.Black), 2);
                g.DrawRectangle(blackBrush, e.X, e.Y, 1, 1);
            }
            if (e.Button == MouseButtons.Right)
            {
                Random random = new Random();
                Color color = Color.FromArgb(255, random.Next(256), random.Next(256), random.Next(256));
                foreach (Point p in points)
                {
                    g.DrawRectangle(new Pen(color, 2), p.X, p.Y, 1, 1);
                }
            }
            pictureBox1.Image = bitmap;
        }
    }
}
